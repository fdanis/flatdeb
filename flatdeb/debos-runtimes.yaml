{{- $apt_debug := or .apt_debug false -}}
{{- $architecture := or .architecture "amd64" -}}
{{- $foreignarchs := or .foreignarchs "" -}}
{{- $suite := or .suite "stretch" -}}
{{- $flatpak_arch := or .flatpak_arch $architecture -}}
{{- $packages := or .packages "" -}}
{{- $pre_apt_script := or .pre_apt_script "" -}}
{{- $post_script := or .post_script "" -}}
{{- $sdk := or .sdk false -}}
{{- $sdk_packages := or .sdk_packages "" -}}
{{- $sdk_post_script := or .sdk_post_script "" -}}
{{- $platform_post_script := or .platform_post_script "" -}}
{{- $strip_source_version_suffix := or .strip_source_version_suffix "" -}}
{{- $build_id := or .build_id "" -}}
{{- $variant := or .variant "" -}}
{{- $variant_id := or .variant_id "" -}}
{{- $ospack := or .ospack (printf "base-%s-%s.tar.gz" $suite $architecture) -}}
{{- $runtime := .runtime -}}
{{- $runtime_branch := or .runtime_branch "master" -}}
{{- $artifact_prefix := or .artifact_prefix (printf "%s-%s-%s" $runtime $flatpak_arch $runtime_branch) -}}
{{- $sysroot_prefix := or .sysroot_prefix (printf "%s-sysroot" $artifact_prefix) -}}
{{- $sysroot_tarball := or .sysroot_tarball "" -}}
{{- $ostree_prefix := or .ostree_prefix (printf "%s-runtime" $artifact_prefix) -}}
{{- $ostree_tarball := or .ostree_tarball (printf "%s.tar.gz" $ostree_prefix) -}}
{{- $sources_directory := or .sources_directory "" -}}
{{- $collect_source_code := or .collect_source_code false -}}
{{- $sources_prefix := or .sources_prefix (printf "%s-sources" $artifact_prefix) -}}
{{- $sources_tarball := or .sources_tarball "" -}}
{{- $debug_symbols := or .debug_symbols false -}}
{{- $automatic_dbgsym := or .automatic_dbgsym false -}}
{{- $debug_prefix := or .debug_prefix (printf "%s-debug" $artifact_prefix) -}}
{{- $debug_tarball := or .debug_tarball (printf "%s.tar.gz" $debug_prefix) -}}

architecture: {{ $architecture }}

actions:
  - action: unpack
    compression: gz
    file: {{ $ospack }}

  # TODO: This duplicates what we should have done in debos-base.yaml
  - action: run
    label: disable-services
    chroot: false
    command: 'echo; "$RECIPEDIR/disable-services" "$ROOTDIR"'

  # TODO: This duplicates what we should have done in debos-base.yaml
  - action: run
    label: clean-up-base
    chroot: false
    command: 'echo; "$RECIPEDIR/clean-up-base" "$ROOTDIR"'

  - action: run
    label: prepare-runtime
    chroot: false
    command: 'echo; "$RECIPEDIR/prepare-runtime" "$ROOTDIR"'

  {{ if $pre_apt_script }}
  - action: run
    label: pre_apt_script
    chroot: true
    script: '{{ $pre_apt_script }}'
  {{ end }}

  {{ if $packages }}
  - action: run
    label: apt-install
    chroot: false
    command: echo; $RECIPEDIR/apt-install {{ if $apt_debug }}--debug{{ end }} "$ROOTDIR" "$RECIPEDIR/runtimes/{{ $runtime }}/packages.yaml"
  {{ end }}

  # TODO: Everything before this step is common to the Platform and SDK

  {{ if $sdk }}
  - action: run
    label: copy manifest from platform
    chroot: false
    command: |
      set -e
      echo
      if [ -e "$RECIPEDIR/manifest.dpkg.platform" ]; then
        cp "$RECIPEDIR/manifest.dpkg.platform" "$ROOTDIR/usr"
      fi
  - action: run
    label: copy manifest from platform
    chroot: false
    command: |
      set -e
      echo
      if [ -e "$RECIPEDIR/manifest.dpkg.built-using.platform" ]; then
        cp "$RECIPEDIR/manifest.dpkg.built-using.platform" "$ROOTDIR/usr"
      fi
  - action: run
    label: copy manifest from platform
    chroot: false
    command: |
      set -e
      echo
      if [ -e "$RECIPEDIR/manifest.platform.deb822.gz" ]; then
        cp "$RECIPEDIR/manifest.platform.deb822.gz" "$ROOTDIR/usr"
      fi
  {{ if and $debug_symbols $automatic_dbgsym }}
  - action: run
    label: collect-dbgsym
    chroot: false
    command: |
      set -e
      echo
      "$RECIPEDIR/collect-dbgsym" {{ if $apt_debug }}--debug{{ end }} "$ROOTDIR"
  {{ end }}
  # We do this as a separate step in case it involves removing any
  # of the common packages, or replacing automatic debug symbols with
  # legacy debug symbols that might have more coverage
  {{ if $sdk_packages }}
  - action: run
    label: apt-install
    chroot: false
    command: echo; $RECIPEDIR/apt-install {{ if $apt_debug }}--debug{{ end }} "$ROOTDIR" "$RECIPEDIR/runtimes/{{ $runtime }}/sdk_packages.yaml"
  {{ end }}
  {{ end }}

  {{ if not $sdk }}
  - action: run
    label: usrmerge
    chroot: false
    command: 'echo; "$RECIPEDIR/usrmerge" "$ROOTDIR"'

  - action: run
    label: platformize
    chroot: false
    command: 'echo; "$RECIPEDIR/platformize" "$ROOTDIR"'
  {{ end }}

  - action: run
    label: purge-conffiles
    chroot: false
    command: 'echo; "$RECIPEDIR/purge-conffiles" "$ROOTDIR"'

  {{ if $post_script }}
  - action: run
    label: post_script
    chroot: true
    script: '{{ $post_script }}'
  {{ end }}

  {{ if $sdk }}
  {{ if $sdk_post_script }}
  - action: run
    label: sdk_post_script
    chroot: true
    script: '{{ $sdk_post_script }}'
  {{ end }}
  {{ else }}
  {{ if $platform_post_script }}
  - action: run
    label: platform_post_script
    chroot: true
    script: '{{ $platform_post_script }}'
  {{ end }}
  {{ end }}

  - action: run
    label: purge-conffiles again
    chroot: false
    command: 'echo; "$RECIPEDIR/purge-conffiles" "$ROOTDIR"'

  - action: run
    label: write-manifest
    chroot: false
    command: |
      set -e
      echo
      "$RECIPEDIR/write-manifest" "$ROOTDIR"
      cp "$ROOTDIR/usr/manifest.dpkg" "$ARTIFACTDIR/{{ $artifact_prefix }}.manifest.dpkg"
      cp "$ROOTDIR/usr/manifest.dpkg.built-using" "$ARTIFACTDIR/{{ $artifact_prefix }}.manifest.dpkg.built-using"
      cp "$ROOTDIR/usr/manifest.deb822.gz" "$ARTIFACTDIR/{{ $artifact_prefix }}.manifest.deb822.gz"

      if test -e "$ROOTDIR/usr/lib/os-release"; then
        "$RECIPEDIR/set-build-id" \
          --build-id="{{ $build_id }}" \
          --variant="{{ $variant }}" \
          --variant-id="{{ $variant_id }}" \
          "$ROOTDIR"

        cp "$ROOTDIR/usr/lib/os-release" "$ARTIFACTDIR/{{ $artifact_prefix }}.os-release.txt"
      fi

  {{ if and $sdk $collect_source_code }}
  - action: run
    label: collect-source-code
    chroot: false
    command: |
      set -e
      echo
      "$RECIPEDIR/collect-source-code" \
        {{ if $sources_directory }}--output="$ARTIFACTDIR/{{ $sources_directory }}"{{ end }} \
        --strip-source-version-suffix="{{ $strip_source_version_suffix }}" \
        "$ROOTDIR"
  {{ end }}

  {{ if not $sdk }}
  - action: run
    label: copy manifest for SDK
    chroot: false
    command: 'echo; cp "$ROOTDIR/usr/manifest.dpkg" "$RECIPEDIR/manifest.dpkg.platform"'
  - action: run
    label: copy manifest for SDK
    chroot: false
    command: 'echo; cp "$ROOTDIR/usr/manifest.dpkg.built-using" "$RECIPEDIR/manifest.dpkg.built-using.platform"'
  - action: run
    label: copy manifest for SDK
    chroot: false
    command: 'echo; mv "$ROOTDIR/usr/manifest.deb822.gz" "$RECIPEDIR/manifest.platform.deb822.gz"'
  - action: run
    label: dpkg --purge dpkg
    chroot: true
    command: 'echo; dpkg --purge --force-remove-essential --force-depends dpkg'
  {{ end }}

  - action: overlay
    source: runtimes/{{ $runtime }}/apt-overlay

  - action: run
    label: save final sources.list
    chroot: false
    command: |
      set -e
      echo
      cp "$ROOTDIR/etc/apt/sources.list" "$ARTIFACTDIR/{{ $artifact_prefix }}.sources.list"

  - action: run
    label: try-apt-get-update
    chroot: true
    command: |
      set -e
      apt-get update || true

  - action: run
    label: put-ldconfig-in-path
    chroot: false
    command: 'echo; "$RECIPEDIR/put-ldconfig-in-path" "$ROOTDIR"'

  - action: run
    label: clean-up-before-pack
    chroot: false
    command: 'echo; "$RECIPEDIR/clean-up-before-pack" "$ROOTDIR"'

  {{ if $sysroot_tarball }}
  - action: run
    label: check uid/gid mapping
    chroot: true
    command: |
      set -e
      echo
      cd "$ROOTDIR"
      if test -n "$(find . -xdev -path ./script -prune -o '(' -uid +99 -o -gid +99 ')' -print)"; then
        echo "Warning: these files have a dynamically-allocated UID/GID"
        echo "         which could cause problems for schroot:"
        find . -xdev -path ./script -prune -o '(' -uid +99 -o -gid +99 ')' -ls
      fi

  - action: run
    label: pack sysroot tarball
    chroot: false
    command: >
      set -e;
      echo;
      tar -cf "$ARTIFACTDIR/{{ $sysroot_tarball }}"
      --numeric-owner
      --sort=name
      --use-compress-program="pigz -n --rsyncable"
      -C "$ROOTDIR"
      --exclude="./script"
      --exclude="./src"
      --exclude="./usr/lib/debug"
      --transform="s,^./,,"
      .
  {{ end }}

  - action: run
    label: usrmerge
    chroot: false
    command: 'echo; "$RECIPEDIR/usrmerge" "$ROOTDIR"'

  - action: overlay
    source: runtimes/{{ $runtime }}/flatpak-overlay

  - action: run
    label: symlink-alternatives
    chroot: false
    command: 'echo; "$RECIPEDIR/symlink-alternatives" "$ROOTDIR"'

  {{ if $debug_symbols }}
  - action: run
    label: dbgsym-use-build-id
    chroot: false
    command: |
      set -eux
      if [ -d "$ROOTDIR/usr/lib/debug" ]; then
        "$RECIPEDIR/dbgsym-use-build-id" --debug-dir "$ROOTDIR/usr/lib/debug"
      fi
  {{ end }}

  {{ if $sdk }}
  - action: run
    label: make-flatpak-friendly --sdk
    chroot: false
    command: 'echo; "$RECIPEDIR/make-flatpak-friendly" --sdk "$ROOTDIR"'
  {{ else }}
  - action: run
    label: make-flatpak-friendly
    chroot: false
    command: 'echo; "$RECIPEDIR/make-flatpak-friendly" "$ROOTDIR"'
  {{ end }}

  # TODO: Move zoneinfo, locales into extensions
  # TODO: Hook point for GL, instead of just Mesa
  # TODO: GStreamer extension
  # TODO: Icon theme, Gtk theme extension
  # TODO: VAAPI extension
  # TODO: SDK extension
  # TODO: ca-certificates extension to get newer certs?

  - action: run
    label: pack ostree tarball
    chroot: false
    command: >
      set -e;
      echo;
      tar -cf "$ARTIFACTDIR/{{ $ostree_tarball }}"
      --numeric-owner
      --sort=name
      --use-compress-program="pigz -n --rsyncable"
      -C "$ROOTDIR"
      files metadata

  {{ if $sdk }}
  {{ if and $collect_source_code $sources_tarball }}
  - action: run
    label: pack source code
    chroot: false
    command: >
      set -e;
      echo;
      tar -cf "$ARTIFACTDIR/{{ $sources_tarball }}"
      --numeric-owner
      --sort=name
      --transform="s,^./,,"
      --use-compress-program="pigz -n --rsyncable"
      -C "$ROOTDIR/src"
      .
  {{ end }}

  {{ if $debug_symbols }}
  - action: run
    label: pack debug symbols
    chroot: false
    command: >
      set -e;
      echo;
      install -d "$ROOTDIR/debug";
      tar -cf "$ARTIFACTDIR/{{ $debug_tarball }}"
      --numeric-owner
      --sort=name
      --transform="s,^./,,"
      --use-compress-program="pigz -n --rsyncable"
      -C "$ROOTDIR/debug"
      .
  {{ end }}

  {{ if $collect_source_code }}
  - action: run
    label: list of included source code
    chroot: false
    command: |
      set -e
      cd "$ARTIFACTDIR"
      dir="{{ or $sources_directory "$ROOTDIR/src/files" }}"
      cp -v "$dir/Sources.gz" "{{ $sources_prefix }}.deb822.gz"
      cp -v "$dir/sources.txt" "{{ $sources_prefix }}.sources.txt"
  {{ end }}

  - action: run
    label: list of missing source code
    chroot: false
    command: >
      set -e;
      cd "$ARTIFACTDIR";
      dir="{{ or $sources_directory "$ROOTDIR/src/files" }}";
      test ! -e "$dir/MISSING.txt" ||
      cp -v "$dir/MISSING.txt"
      "{{ $sources_prefix }}.MISSING.txt"
  {{ end }}

  - action: run
    label: manifests
    chroot: false
    command: >
      set -e;
      cd "$ARTIFACTDIR";
      head -n10000
      "$ROOTDIR/files/manifest.dpkg"
      "$ROOTDIR/files/manifest.dpkg.built-using"
      {{ or $sources_directory "$ROOTDIR/src/files" }}/sources.txt
      || true

  - action: run
    label: metadata
    chroot: false
    command: >
      set -e;
      cd "$ARTIFACTDIR";
      head -n10000
      "$ROOTDIR/files/etc/apt/apt.conf"
      "$ROOTDIR/files/etc/apt/apt.conf.d"/*
      "$ROOTDIR/files/etc/apt/sources.list"
      "$ROOTDIR/files/etc/apt/sources.list.d"/*
      "$ROOTDIR/files/etc/debian_chroot"
      "$ROOTDIR/files/lib/os-release"
      {{ or $sources_directory "$ROOTDIR/src/files" }}/MISSING.txt
      "$ROOTDIR/metadata"
      || true
